# Graphic User Interface Module

import curses
import curses.panel
import time
from gamepieces import dice as gp


def draw_scoring(window, pos_y, pos_x):
    """Displays the scoring system in a small tally display"""
    cur_y, cur_x = window.getmaxyx()
    sub_title = "Word Length"
    score_win = window.derwin(9, 13, pos_y, pos_x)
    score_win.bkgd(curses.color_pair(1))
    score_win.addstr(1, 1, sub_title, curses.A_UNDERLINE)

    wd_len = [
            " 3 =  1pt",
            " 4 =  1pt",
            " 5 =  2pts",
            " 6 =  3pts",
            " 7 =  5pts",
            "8+ = 11pts"
            ]

    wd_y = 2
    for idx in range(0, len(wd_len)):
        score_win.addstr(wd_y, 2, wd_len[idx])
        wd_y += 1


def goodbye(stdscr):
    """Clears the screen and prints Goodbye on exit"""
    stdscr.nodelay(False)
    y, x = stdscr.getmaxyx()
    goodbye = "GOODBYE!"
    stdscr.clear()
    stdscr.box()
    stdscr.bkgd(curses.color_pair(7))
    stdscr.addstr((y - 1) // 2, (x - len(goodbye)) // 2, goodbye)
    stdscr.refresh()
    time.sleep(2)
    exit()


class DrawBoard:
    """Class builds and draws the gameboards tiles"""
    def __init__(self, stdscr, cur_board, game_mode):
        self._stdscr = stdscr
        self._game = cur_board
        self._scr_y, self._scr_x = self._stdscr.getmaxyx()
        self._game_mode = game_mode

    @property
    def place_board(self):
        """Draws the board onto the game screen"""
        shadow = self.board_caddy(14, 24)
        shadow.bottom()

        dice_board = self._stdscr.derwin(
                14,
                24,
                5,
                int((self._scr_x - 24) // 2)
                )
        dice_board.clear()
        dice_board.box()
        dice_board.bkgd(curses.color_pair(1))

        # Generators to yield a die and a location to place it
        dice_gen = self._game.cur_die()
        grid_loc = self.grid_coord(1, 2)

        for i in range(0, 16):
            self.make_cell(
                    dice_board,
                    next(grid_loc),
                    next(dice_gen)
                    )

    @property
    def play_game(self):
        """Creates the gameloop and handles the game flow and input of
        answers"""

        self.place_board
        self._clock = self._stdscr.derwin(1, 9, 3, int((self._scr_x - 9) / 2))
        self._clock.bkgd(curses.color_pair(2))

        if self._game_mode is 1:  # 1-player
            self._pc = gp.Player()
            self._usr = gp.Player()
            self.draw_1player
            self.play_1plyr
        else:  # 2-player
            self.draw_2player
            self.play_2plyr

    @property
    def draw_1player(self):
        self._word_win = 18
        self._stdscr.addstr(
                1,
                int((self._scr_x - self._word_win) / 10),
                "Player 1", curses.A_UNDERLINE + curses.color_pair(1)
                )
        self._p1_guess = self._stdscr.derwin(
                3,
                25,
                20,
                int((self._scr_x - 24) / 2)
                )
        self._p1_guess.nodelay(True)
        self._p1_guess.box()

        # Create window to display players words
        self._p1_words = self._stdscr.derwin(
                21,
                self._word_win,
                2,
                int((self._scr_x - self._word_win) / 10)
                )
        self._p1_words.box()
        self._p1_words.bkgd(curses.color_pair(1))
        self._p1_words.noutrefresh()

        self._stdscr.addstr(
                1,
                int((self._scr_x - self._word_win) * 9 / 10) + 2,
                "Computer",
                curses.A_UNDERLINE + curses.color_pair(3)
                )

        # Create window to display computers words
        self._pc_words = self._stdscr.derwin(
                21,
                self._word_win,
                2,
                int((self._scr_x - self._word_win) * 9 / 10) + 2
                )
        self._pc_words.box()
        self._pc_words.bkgd(curses.color_pair(3))
        self._pc_words.noutrefresh()

        self._stdscr.refresh()

    @property
    def draw_2player(self):
        draw_scoring(self._stdscr, self._scr_y // 4, self._scr_x // 10)

    @property
    def play_1plyr(self):
        cur_time = time.clock()
        pc_time = time.clock()

        # Initial cursor positions for word output
        pc_xpos, pc_ypos = 1, 1
        usr_xpos, usr_ypos = 1, 1

        guess = ""
        cursor_pos = 1

        wd_color = 3
        self._stdscr.refresh()

        # Game loop begin
        while self._game.gametime >= 0 or self._game.answers is []:
            # Score output
            self._stdscr.addstr(
                    1,
                    int((self._scr_x - self._word_win) / 10) + 13,
                    "{0:03}".format(self._usr.score),
                    curses.color_pair(2)
                    )
            self._stdscr.addstr(
                    1,
                    int((self._scr_x - self._word_win) * 9 / 10) + 15,
                    "{0:03}".format(self._pc.score),
                    curses.color_pair(2)
                    )
            self._stdscr.refresh()

            # Clock output
            gmin = int(self._game.gametime / 60)
            gsec = int(self._game.gametime % 60)
            self._clock.addstr(
                    0,
                    2,
                    "{0:02}:{1:02}".format(gmin, gsec),
                    curses.color_pair(2)
                    )
            self._clock.refresh()
            if ((time.clock() - cur_time) > 1):
                self._game._gametime -= 1
                cur_time = time.clock()

            # PC Timed guess
            if ((time.clock() - pc_time) >= self._game.pc_time):
                # PC guess
                if self._game.is_answer is False:
                    break
                cur_wd = self._game.get_word
                self._pc_words.addstr(
                        pc_ypos,
                        pc_xpos,
                        cur_wd,
                        curses.color_pair(wd_color)
                        )

                self._pc_words.addstr(
                        0,
                        14,
                        "+{0}".format(self.score_word(cur_wd))
                        )
                self._pc_words.refresh()
                pc_time = time.clock()

                self._pc.new_word(cur_wd)
                pc_ypos += 1

                # Verify positioning of word output
                if pc_ypos is 20:
                    if pc_xpos is 10:
                        pc_ypos = 1
                        pc_xpos = 1
                        wd_color += 1
                    else:
                        pc_ypos = 1
                        pc_xpos = 10

            # User guesses
            usr_letter = self._p1_guess.getch(1, cursor_pos)
            if usr_letter == -1:
                continue

            # ESC key allows early exit from game
            if usr_letter == 27:
                break

            # Condition submits word as is
            elif usr_letter == 10 or usr_letter == 32 or cursor_pos > 14:
                for i in range(1, cursor_pos + 1):
                    self._p1_guess.addstr(1, i, " ")
                cursor_pos = 1
                if guess == "":
                    continue

                if self._game.check_answer(guess) == True:
                    if usr_ypos is 20:
                        if usr_xpos is 10:
                            usr_ypos = 1
                            usr_xpos = 2
                        else:
                            usr_ypos = 1
                            usr_xpos = 10

                    # Add word to player list
                    self._usr.new_word(guess)
                    self._p1_words.addstr(usr_ypos, usr_xpos, guess)
                    self._p1_words.addstr(
                            0,
                            14,
                            "+{0}".format(self.score_word(guess))
                            )
                    self._p1_words.refresh()
                    usr_ypos += 1

                guess = ""

            # Backspace management
            elif usr_letter == 263 or usr_letter == 127:
                guess = guess[:-1]
                cursor_pos -= 1
                if cursor_pos < 1:
                    cursor_pos = 1
                    continue

                # Verify positioning of word output
                if usr_xpos is 20:
                    if usr_ypos is 10:
                        usr_xpos = 1
                        usr_ypos = 2
                    else:
                        usr = 1
                        usr_ypos = 10
                self._p1_guess.addstr(1, cursor_pos, " ")
            else:
                self._p1_guess.addstr(1, cursor_pos, chr(usr_letter))
                cursor_pos += 1

                guess += chr(usr_letter)

            self._p1_guess.refresh()
            curses.doupdate()

        self._p1_guess.clear()
        self._p1_guess.bkgd(curses.color_pair(2))
        self._p1_guess.addstr(1, 0, " GAME OVER (Press Enter)", curses.A_BOLD)
        self._p1_guess.refresh()

        # Wait for Enter key
        while True:
            key = self._stdscr.getch()
            if key == 10:
                break
            elif key == 27 or key == ord('q'):
                goodbye(self._stdscr)

        # Output Results
        self.get_results_1plyr(self._usr, self._pc)

        while True:
            key = self._stdscr.getch()
            if key == 27 or key == 10:
                break

    @property
    def play_2plyr(self):
        cur_time = time.clock()
        self._stdscr.refresh()
        self._stdscr.nodelay(True)
        # Game loop begin
        while self._game.gametime >= 0:
            gmin = int(self._game.gametime / 60)
            gsec = int(self._game.gametime % 60)
            self._clock.addstr(
                    0,
                    2,
                    "{0:02}:{1:02}".format(gmin, gsec),
                    curses.color_pair(2)
                    )
            self._clock.refresh()

            if ((time.clock() - cur_time) > 1):
                self._game._gametime -= 1
                cur_time = time.clock()
            key = self._stdscr.getch()

            # Condition to allow early exit of game
            if key == 27 or key == ord('q'):
                break
        timeout = "GAME OVER! (PRESS ENTER TO CONTINUE)"
        timewin = self._stdscr.derwin(
                5,
                len(timeout) + 4,
                ((self._scr_y - 1) // 2) - 1,
                ((self._scr_x - len(timeout)) // 2) - 2
                )
        timewin.clear()
        timewin.bkgd(curses.color_pair(2))
        timewin.addstr(2, 2, timeout, curses.color_pair(2) + curses.A_BOLD)
        timewin.refresh()
        # Wait for Enter key
        while True:
            key = self._stdscr.getch()
            if key == 10:
                break

        # Output words from board
        self.get_results_2plyr()

        while True:
            key = self._stdscr.getch()
            if key == 27 or key == 10:
                break

    def board_caddy(self, rows, cols):
        """Creates the shadow backdrop for the games dice"""
        dice_caddy = self._stdscr.derwin(
                rows,
                cols,
                5,
                int((self._scr_x - cols) / 2)
                )
        dice_caddy.box()
        dice_caddy.bkgd(curses.color_pair(1))
        panel = curses.panel.new_panel(dice_caddy)

        shadow = self._stdscr.derwin(
                rows,
                cols,
                6,
                int((self._scr_x - cols) / 2) + 2
                )
        shadow.box()
        shadow.bkgd(curses.color_pair(4))
        shadpnl = curses.panel.new_panel(shadow)
        return shadpnl

    def make_cell(self, parent, coords, letter):
        """Creates the individual cell for each dice"""
        win = parent.derwin(3, 5, coords[0], coords[1])
        win.erase()
        win.box()
        win.bkgd(curses.color_pair(1))
        win.addstr(1, 2, letter)

    def grid_coord(self, row_start, col_start):
        """Generator yields the current placement for the tile"""
        row = row_start
        col = col_start
        for r in range(row, row + 10, 3):
            for c in range(col, col + 16, 5):
                yield r, c

    def get_results_1plyr(self, player, computer):
        win_heights = self._scr_y // 3
        """Produces the final windows output and displays it to a
        cleared screen"""
        # Player 1 results
        self._stdscr.clear()
        p1_result = self._stdscr.derwin(win_heights - 1, self._scr_x, 0, 0)
        p1_result.bkgd(curses.color_pair(1))
        p1_result.addstr(
                1,
                1,
                "Player Words - Score: {0}".format(player.score),
                curses.A_BOLD
                )

        if player.score > computer.score:
            p1_result.addstr(
                    1,
                    int((self._scr_x - 8) / 2),
                    " WINNER ",
                    curses.A_BOLD + curses.color_pair(2)
                    )
        else:
            p1_result.addstr(
                    1,
                    int((self._scr_x - 8) / 2),
                    " LOSER! ",
                    curses.A_BOLD + curses.color_pair(2)
                    )

        pos_y, pos_x = 2, 2

        # Output words with line wrapping
        for word in sorted(player.answers):
            if pos_x + len(word) > self._scr_x - 1:
                pos_x = 2
                pos_y += 1
            p1_result.addstr(pos_y, pos_x, word)
            pos_x += len(word) + 1
            if pos_y >= win_heights - 2:
                break
        p1_result.box()
        p1_result.refresh()

        # Missed words
        pos_y, pos_x = 2, 2
        missed = self._stdscr.derwin(
                win_heights - 1,
                self._scr_x,
                win_heights,
                0
                )
        missed.addstr(1, 1, "Missed Words", curses.A_BOLD)

        # Output words with line wrapping
        for word in sorted(self._game.answers):
            if pos_x + len(word) > 79:
                pos_x = 2
                pos_y += 1
            missed.addstr(pos_y, pos_x, word)
            pos_x += len(word) + 1
            if pos_y >= win_heights - 2:
                break
        missed.box()
        missed.refresh()

        # PC results
        pos_y, pos_x = 2, 2
        pc_result = self._stdscr.derwin(
                (self._scr_y - (win_heights * 2)) - 1,
                self._scr_x, win_heights * 2, 0
                )
        pc_result.bkgd(curses.color_pair(3))

        pc_result.addstr(
                1,
                1,
                "Computer Words - Score: {0}".format(computer.score),
                curses.A_BOLD
                )
        if computer.score > player.score:
            pc_result.addstr(
                    1,
                    int((self._scr_x - 8) / 2),
                    " WINNER ",
                    curses.A_BOLD + curses.color_pair(2)
                    )
        else:
            pc_result.addstr(
                    1,
                    int((self._scr_x - 8) / 2),
                    " LOSER! ",
                    curses.A_BOLD + curses.color_pair(2)
                    )

        pos_y, pos_x = 2, 2

        # Output words with line wrapping
        for word in sorted(computer.answers):
            if pos_x + len(word) > 79:
                pos_x = 2
                pos_y += 1
            pc_result.addstr(pos_y, pos_x, word)
            pos_x += len(word) + 1
            if pos_y >= win_heights - 2:
                break
        pc_result.box()
        pc_result.refresh()

        endscreen = "(Press Enter to Continue)"
        self._stdscr.addstr(
                0,
                (self._scr_x - len(endscreen)) // 2,
                endscreen,
                curses.A_BOLD + curses.color_pair(2)
                )

    def get_results_2plyr(self):
        """Produces the final windows output and displays it to a
        cleared screen"""
        # Player 1 results
        self._stdscr.clear()
        self._stdscr.box()
        self._stdscr.bkgd(curses.color_pair(1))
        self._stdscr.addstr(1, 1, "Game Answers:", curses.A_BOLD)
        endscreen = "(Press Enter to Continue)"
        self._stdscr.addstr(
                1,
                (self._scr_x - len(endscreen)) // 2,
                endscreen,
                curses.A_BOLD + curses.color_pair(2)
                )

        pos_y, pos_x = 2, 2

        # Output words with point value
        for word in sorted(self._game.answers):
            self._stdscr.addstr(
                    pos_y,
                    pos_x,
                    "{0:02}-{1}".format(self.score_word(word), word)
                    )
            pos_y += 1
            if pos_y > self._scr_y - 2:
                pos_y = 2
                pos_x += 12
            if pos_x + 12 > self._scr_x - 3:
                break
        self._stdscr.refresh()

    def score_word(self, word):
        if len(word) < 5:
            return 1
        elif len(word) == 5:
            return 2
        elif len(word) == 6:
            return 3
        elif len(word) == 7:
            return 5
        else:
            return 11


class MenuScreen:
    """Creates the Boggle welcome screen and handles the users selections
    prior to playing the game"""

    def __init__(self, stdscr):
        self._stdscr = stdscr
        self._scr_y, self._scr_x = self._stdscr.getmaxyx()
        self._selection = 0
        self._title = "BOGGLE"
        self._title_keys = []
        self._title_win = curses.newwin(60, 200, 4, (self._scr_x - 41) // 2)
        self._key = 0

    def start_screen(self):
        """Start point for the menu screen"""
        self._stdscr.refresh()
        self._make_title()
        self._make_selections()
        return self.player_select()

    def player_select(self):
        """Handles the selection of one player or two player"""
        selection = 1
        while True:
            # 1 = 1-player, 2 = 2-player
            key = self._stdscr.getch()
            if key == 261:
                self._one_player.bkgd(curses.color_pair(3))
                self._two_player.bkgd(curses.color_pair(2))
                self._one_player.refresh()
                self._two_player.refresh()
                selection = 2
            elif key == 260:
                self._one_player.bkgd(curses.color_pair(2))
                self._two_player.bkgd(curses.color_pair(3))
                self._one_player.refresh()
                self._two_player.refresh()
                selection = 1
            if key == 10:
                return selection
            elif key == 27 or key == ord('q'):
                goodbye(self._stdscr)

    def _make_title(self):
        """Creates the welcome tiles with 'Boggle' in them"""
        spacing = 4
        header = "Welcome to"
        self._stdscr.addstr(
                5,
                (self._scr_x - len(header)) // 2,
                header,
                curses.color_pair(3)
                )
        self._stdscr.refresh()
        for letter in self._title:
            self._title_keys.append(self._title_win.derwin(3, 5, 4, spacing))
            spacing += 6
        for idx, letter in enumerate(self._title_keys):
            letter.box()
            letter.bkgd(curses.color_pair(3), curses.A_BOLD)
            letter.addstr(1, 2, self._title[idx])
            letter.refresh()

        escape_msg = "('esc' or 'q' to exit)"
        self._stdscr.addstr(
                2,
                (self._scr_x - len(escape_msg)) // 2,
                escape_msg,
                curses.A_BOLD + curses.color_pair(2)
                )

    def _make_selections(self):
        """Creates the player selection windows"""
        self._one_player = self._title_win.derwin(3, 14, 10, 5)
        self._one_player.box()
        self._one_player.bkgd(curses.color_pair(2))
        self._one_player.addstr(1, 3, "1-Player")
        self._one_player.refresh()

        self._two_player = self._title_win.derwin(3, 14, 10, 25)
        self._two_player.box()
        self._two_player.bkgd(curses.color_pair(3))
        self._two_player.addstr(1, 3, "2-Player")
        self._two_player.refresh()


class GameSettings:
    def __init__(self, stdscr, mode):
        self._stdscr = stdscr
        self._scr_y, self._scr_x = self._stdscr.getmaxyx()
        self._gamemode = mode
        self._gametime = 180
        self._pc_time = 7
        self._dict = "/usr/share/dict/american-english"

    @property
    def gametime(self):
        """Returns the game time"""
        return self._gametime

    @property
    def pc_time(self):
        """Returns the pcs turn time"""
        return self._pc_time

    @property
    def dict_path(self):
        """Returns the dictionary path"""
        return self._dict

    def get_settings(self):
        if self._gamemode is 1:
            self.get_p1_settings()
        else:
            self.get_p2_settings()

    def get_p1_settings(self):
        """Allows user to select time to change and input new values before
        a 1-player game"""
        self.draw_settings()

        start_button = self._stdscr.derwin(
                3,
                16,
                int((self._scr_y / 4) * 3),
                int((self._scr_x - 15) / 2)
                )
        start_button.box()
        start_button.bkgd(curses.color_pair(3))
        start_str = "START GAME"
        start_button.addstr(1, int((16 - len(start_str)) // 2), start_str)
        self._win_select.append(start_button)

        self._stdscr.refresh()

        for i in self._win_select:
            i.bkgd(curses.color_pair(7))

        cur_select = 0
        prv_select = 1
        while True:
            self._stdscr.refresh()
            self._win_select[cur_select].bkgd(curses.color_pair(7))
            self._win_select[cur_select].refresh()
            self._win_select[prv_select].bkgd(curses.color_pair(3))
            self._win_select[prv_select].refresh()

            if prv_select != cur_select:
                prv_select = cur_select

            key_in = self._stdscr.getch()
            if key_in == 27 or key_in == ord('q'):
                goodbye(self._stdscr)

            if key_in == 258:
                cur_select += 1
            elif key_in == 259:
                cur_select -= 1
            elif key_in == 10:
                # 0 = Game clock, Size = 1, 6
                if cur_select == 0:
                    self._win_select[cur_select].clear()
                    self._gametime = self.assign_time(
                            self._win_select[cur_select]
                            )

                # 1 = PC clock, Size = 1, 6
                elif cur_select == 1:
                    self._win_select[cur_select].clear()
                    self._pc_time = self.assign_time(
                            self._win_select[cur_select]
                            )

                # 2 = Dictionary path, Size = 1, 40 FUTURE IMPLEMENTATION
                # elif cur_select == 2:
                #    pass
                # 3 = Game start button
                elif cur_select == 2:
                    # Verify valid settings and then break to game
                    break

            if cur_select > len(self._win_select) - 1:
                cur_select = 0
            elif cur_select < 0:
                cur_select = len(self._win_select) - 1

    def get_p2_settings(self):
        """Allows user to select time to change and input new values before
        a 1-player game"""
        self.draw_settings()

        start_button = self._stdscr.derwin(
                3,
                16,
                int((self._scr_y / 4) * 3),
                int((self._scr_x - 15) / 2)
                )
        start_button.box()
        start_button.bkgd(curses.color_pair(3))
        start_str = "START GAME"
        start_button.addstr(1, int((16 - len(start_str)) // 2), start_str)
        self._win_select.append(start_button)

        self._stdscr.refresh()

        for i in self._win_select:
            i.bkgd(curses.color_pair(7))

        cur_select = 0
        prv_select = 1
        while True:
            self._stdscr.refresh()
            self._win_select[cur_select].bkgd(curses.color_pair(7))
            self._win_select[cur_select].refresh()
            self._win_select[prv_select].bkgd(curses.color_pair(3))
            self._win_select[prv_select].refresh()

            if prv_select != cur_select:
                prv_select = cur_select

            key_in = self._stdscr.getch()
            if key_in == 27 or key_in == ord('q'):
                goodbye(self._stdscr)
            if key_in == 258:
                cur_select += 1
            elif key_in == 259:
                cur_select -= 1
            elif key_in == 10:
                # 0 = Game clock, Size = 1, 6
                if cur_select == 0:
                    self._win_select[cur_select].clear()
                    self._gametime = self.assign_time(
                            self._win_select[cur_select]
                            )

                # 1 = Dictionary path, Size = 1, 40 FUTURE IMPLEMENTATION
                # elif cur_select == 1:
                #    pass
                # 2 = Game start button
                elif cur_select == 1:
                    break

            if cur_select > len(self._win_select) - 1:
                cur_select = 0
            elif cur_select < 0:
                cur_select = len(self._win_select) - 1

    def get_time_input(self, window, y, x, maxlen):
        """Used to grab a users time input, y/x are location to get it.
        Returns string with valid numerical characters"""
        curses.noecho()
        curses.cbreak()
        temp_str = ""
        temp_idx = x
        while True:
            key = window.getch(y, temp_idx)
            window.refresh()
            if temp_idx >= maxlen:
                temp_idx = maxlen - 1
                temp_str = temp_str[:-1]
                window.addstr(y, maxlen - 1, " ")
                continue
            if key == 10 or key == 32 or key == 27:
                if temp_str is "":
                    continue
                else:
                    return temp_str
            elif key == 263 or key == 127:
                temp_str = temp_str[:-1]
                temp_idx -= 1
                if temp_idx < 0:
                    temp_idx = 0
                    continue
                window.addstr(y, temp_idx, " ")
            elif key >= 46 and key <= 58:
                if key is 47:
                    continue
                window.addstr(y, temp_idx, chr(key))
                temp_idx += 1
                temp_str += chr(key)

    def assign_time(self, cur_win):
        """Gets string input value for time, splits it and converts to
        seconds. Returns new gametime in whole seconds"""
        temp_time = self.get_time_input(
                cur_win,
                0,
                0,
                5
                )

        if ":" in temp_time:
            times = temp_time.split(":")
        elif "." in temp_time:
            times = temp_time.split(".")
        else:
            return int(temp_time)

        if len(times) == 1:
            if times[0] < 0:
                return 0
            return int(times[0])
        elif len(times) >= 2:
            if times[0] is '' and times[1] is '':
                return 0
            if times[0] is '' and times[1] is not '':
                return int(times[1])
            elif times[0] is not '' and times[1] is '':
                return int(times[0]) * 60
            else:
                return int(times[0]) * 60 + int(times[1])

    def draw_settings(self):
        """Displays prompts for user to change default settings"""
        self._stdscr.clear()
        self._stdscr.box()
        self._win_select = []

        settings_y = 11
        settings_x = 44

        scoring_y = int(self._scr_y / 3)
        scoring_x = int(self._scr_x / 3)

        self._settings = self._stdscr.derwin(
                settings_y,
                settings_x,
                int((self._scr_y - settings_y) // 2),
                int((self._scr_x - settings_x) // 2)
                )
        self._settings.bkgd(curses.color_pair(1))
        sub_title = "Settings"
        self._settings.addstr(
                1,
                (settings_x - len(sub_title)) // 2,
                sub_title,
                curses.A_UNDERLINE + curses.color_pair(1)
                )

        self._settings.addstr(3, 2, "{0:<15}:".format("Game Time"))

        g_time = self._settings.derwin(1, 6, 3, 18)
        g_time.bkgd(curses.color_pair(3))
        g_time.addstr(
                0,
                0,
                "{0:02}:{1:02}".format(
                    int(self._gametime / 60),
                    int(self._gametime % 60))
                )
        g_time.noutrefresh()
        self._win_select.append(g_time)

        if self._gamemode is 1:
            self._settings.addstr(5, 2, "{0:<15}:".format("Computer Pace"))
            pc_time = self._settings.derwin(1, 6, 5, 18)
            pc_time.bkgd(curses.color_pair(3))
            pc_time.addstr(
                    0,
                    0,
                    "{0:02}:{1:02}".format(
                        int(self._pc_time / 60),
                        int(self._pc_time % 60))
                    )
            self._win_select.append(pc_time)

        self._settings.addstr(7, 2, "Dictionary:")
        dict_path = self._settings.derwin(1, 40, 8, 1)
        dict_path.addstr(0, 0, "{0}".format(self._dict))
        dict_path.bkgd(curses.color_pair(3))
        # User modified dictionary path for future implementation
        # self._win_select.append(dict_path)

        scr_title = "Scoring"
        scoring = self._stdscr.derwin(
                settings_y,
                scoring_x,
                2,
                settings_x + 4
                )
        scoring.addstr(
                1,
                (scoring_x - len(scr_title)) // 2,
                scr_title,
                curses.A_UNDERLINE + curses.color_pair(1)
                )

        draw_scoring(scoring, 1, 6)
        escape_msg = "('esc' or 'q' to exit)"
        self._stdscr.addstr(
                2,
                (self._scr_x - len(escape_msg)) // 2,
                escape_msg,
                curses.A_BOLD + curses.color_pair(2)
                )
        self._stdscr.refresh()
