import curses, traceback

def main(stdscr):
    # Frame the interface area at fixed VT100 size
    stdscr.clear()
    stdscr = curses.initscr()

    # screen.addstr("\n\tFirst string\n\n")

    stdscr.addstr(12, 2, 'This is a test', curses.A_BOLD)
    stdscr.addstr(13, 2, 'This is a test', curses.A_DIM)
    stdscr.addstr(15, 2, 'This is a test', curses.A_STANDOUT)
    stdscr.addstr(16, 2, 'This is a test', curses.A_UNDERLINE)

    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
    stdscr.addstr(18, 2, 'This is a test', curses.color_pair(1))

    stdscr.border()
    stdscr.refresh()
    stdscr.getkey()

    usr_in = curses.newwin(5,5, 11, 5)
    usr_in.box()
    usr_in.addstr(1, 2, "I", hotkey_attr)
    usr_in.addstr(1, 3, "nput", menu_attr)
    usr_in.addstr(2, 2, "O", hotkey_attr)
    usr_in.addstr(2, 3, "utput", menu_attr)
    usr_in.addstr(3, 2, "T", hotkey_attr)
    usr_in.addstr(3, 3, "ype", menu_attr)
    usr_in.addstr(1, 2, "", hotkey_attr)
    usr_in.refresh()
    c = usr_in.getch()
    if c in (ord('I'), ord('i'), curses.KEY_ENTER, 10):
        curses.echo()
        usr_in.erase()
        stdscr.addstr(5, 33, " "*43, curses.A_UNDERLINE)
        cfg_dict['source'] = screen.getstr(5, 33)
        curses.noecho()
    else:
        curses.beep()
        usr_in.erase()
    return CONTINUE


    """file_menu = ("File", None)
    sett_menu = ("Settings", None)
    rule_menu = ("Rules", None)
    exit_menu = ("Exit", "quit()")
    topbar_menu = ((file_menu, sett_menu, rule_menu, exit_menu))

    while topbar_key_handler():
        draw_dict()
"""





    while True:
        event = stdscr.getch()
        if event == ord("q"):
            break

    curses.endwin()

if __name__=='__main__':
    curses.wrapper(main)
