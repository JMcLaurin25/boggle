#!/usr/bin/python3

import curses
import curses.textpad
import os
import sys
from gui_elem import gui_elem as ge
from gamepieces import dice as gp


def main(stdscr):
    progloop = True

    y, x = stdscr.getmaxyx()
    curses.init_pair(1, curses.COLOR_BLUE, curses.COLOR_BLACK)
    curses.init_pair(2, curses.COLOR_RED, curses.COLOR_WHITE)
    curses.init_pair(3, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(4, curses.COLOR_YELLOW, curses.COLOR_BLACK)
    curses.init_pair(5, curses.COLOR_MAGENTA, curses.COLOR_BLACK)
    curses.init_pair(6, curses.COLOR_CYAN, curses.COLOR_BLACK)
    curses.init_pair(7, curses.COLOR_WHITE, curses.COLOR_RED)

    # Game description
    stdscr.clear()
    stdscr.box()
    stdscr.bkgd(curses.color_pair(1))
    disclaimer = "Disclaimer:\nThis version of Boggle is modeled after the "
    disclaimer += "1976 -Yellow Box- UK version, distributed by Parker Bros."
    disclaimer += "\n\nThe game begins with a tray of 16 cubic dice, each with"
    disclaimer += " a different letter printed on each of its sides. Each dice"
    disclaimer += " is randomly placed into the 4x4 tray. A three-minute timer"
    disclaimer += " is started and the players simultaneously begin the game."
    disclaimer += "\n\tEach player searches for words that can be constructed "
    disclaimer += "from the letters of sequentially adjacent dice (adjacent "
    disclaimer += "cubes are those horizontally, vertically, and diagonally "
    disclaimer += "neighboring). Words must be at least three letters long, "
    disclaimer += "may include singular and plural (or other derived forms) "
    disclaimer += "separately, but may not use the same letter cube more than "
    disclaimer += "once per word.\n\n https://en.wikipedia.org/wiki/Boggle"
    disclaimer += "\t(Any Key to Continue) "

    introwin = stdscr.derwin((y * 3) // 4, (x * 7) // 8, y // 8, x // 16)
    curses.textpad.Textbox(introwin)
    introwin.addstr(1, 1, disclaimer)

    stdscr.getch()

    # Program loop to allow sequential game play
    while progloop is True:
        stdscr.clear()
        stdscr.border()
        menuScreen = ge.MenuScreen(stdscr)
        game_mode = menuScreen.start_screen()

        game_settings = ge.GameSettings(stdscr, game_mode)
        game_settings.get_settings()

        new_game = gp.GameBoard(
                game_settings.dict_path,
                game_settings.gametime,
                game_settings.pc_time
                )

        stdscr.clear()

        title = "Boggle"
        title_center = int((x - len(title)) / 2)
        stdscr.addstr(
                1,
                title_center,
                "Boggle",
                curses.A_BOLD + curses.A_UNDERLINE + curses.color_pair(1)
                )

        # Game Board Creation
        cur_board = ge.DrawBoard(stdscr, new_game, game_mode)

        # Draws game board and initiates the game loop
        cur_board.play_game

if __name__ == '__main__':
    # os.environ['TERM'] = 'putty'
    if sys.version_info[0] < 3:
        sys.stderr.write("\n - Must be using Python 3! - \n\n")
        exit()
    try:
        curses.wrapper(main)
    except:
        print("Application Exited, adjust window size and relaunch")
