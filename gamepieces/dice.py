import math
import random
import re
import time


class Player:
    """The player class stores the players score and their answers"""
    def __init__(self):
        self._score = 0
        self._answers = []

    @property
    def score(self):
        return self._score

    def score_word(self, word):
        if len(word) < 5:
            self._score += 1
        elif len(word) == 5:
            self._score += 2
        elif len(word) == 6:
            self._score += 3
        elif len(word) == 7:
            self._score += 5
        else:
            self._score += 11

    @property
    def answers(self):
        return self._answers

    def new_word(self, word):
        self.score_word(word)
        self._answers.append(word)


class GameBoard:
    """GameBoard class contains all elements of the game board, including
    methods to generate the board, find all solutions and display data
    Solution derived from: http://stackoverflow.com/a/750012. Board dice
    distribution derived from 1976 Boggle version from Parker Bros.
    http://www.boardgamegeek.com/thread/300565/review-boggle-veteran-and
    -beware-different-version """

    dice = {
            1: ['a', 'a', 'c', 'i', 'o', 't'],
            2: ['a', 'h', 'm', 'o', 'r', 's'],
            3: ['e', 'g', 'k', 'l', 't', 'y'],
            4: ['a', 'b', 'i', 'l', 't', 'y'],
            5: ['a', 'c', 'd', 'e', 'm', 'p'],
            6: ['e', 'g', 'i', 'n', 't', 'v'],
            7: ['g', 'i', 'l', 'r', 'u', 'w'],
            8: ['e', 'l', 'p', 's', 't', 'u'],
            9: ['d', 'e', 'n', 'o', 's', 'w'],
            10: ['a', 'c', 'e', 'l', 'r', 's'],
            11: ['a', 'b', 'j', 'm', 'o', 'q'],
            12: ['e', 'e', 'f', 'h', 'i', 'y'],
            13: ['e', 'h', 'i', 'n', 'p', 's'],
            14: ['d', 'k', 'n', 'o', 't', 'u'],
            15: ['a', 'd', 'e', 'n', 'v', 'z'],
            16: ['b', 'i', 'f', 'o', 'r', 'x']
            }

    def __init__(self, dictionary, game, pc):
        self._dict = dictionary
        self._gametime = game
        self._pc = pc
        self._game_board = ""
        self.gen_gameboard()
        self._game_grid = self._game_board.split()
        self._rows = len(self._game_grid)
        self._cols = len(self._game_grid[0])
        self._words = self.get_words()
        self._prefixes = self.get_prefixes()
        self._answers = self.get_answers()
        self._found = []

    @property
    def gametime(self):
        return self._gametime

    @property
    def pc_time(self):
        return self._pc

    @property
    def answers(self):
        return self._answers

    @property
    def is_answer(self):
        if self._answers != []:
            return True
        return False

    @property
    def get_word(self):
        word = random.choice(self._answers)
        self._answers.remove(word)
        self._found.append(word)
        return word

    def check_answer(self, guess):
        """Check a single guess against the board, remove from list, add to
        found"""
        if guess in self._answers:
            self._answers.remove(guess)
            self._found.append(guess)
            return True
        return False

    def gen_gameboard(self):
        """Generates the gameboard with random die and random die side"""
        idx = 0
        random.seed(time.clock())
        dice = [die for die in self.dice.keys()]
        spot = [num for num in range(0, 16)]
        while dice != []:
            idx += 1
            cur_die = random.choice(dice)
            dice.remove(cur_die)
            cur_spot = random.choice(spot)
            cur_letter = random.choice(self.dice[cur_die])
            self._game_board += cur_letter
            if idx % 4 == 0:
                self._game_board += " "
            spot.remove(cur_spot)

    def cur_die(self):
        """Die generator"""
        board = ''.join(self._game_board.split())
        for i in range(0, len(board)):
            yield board[i]

    def get_words(self):
        """Solution derived from http://stackoverflow.com/a/750012"""
        words = []
        letters = ''.join(self._game_board)
        onboard = re.compile('[' + letters + ']{3,}$', re.I).match

        # Find potential words from board. Still not
        # measuring adjacencies
        for word in open(self._dict):
            if onboard(word) and len(word) > 2:
                words.append((word.rstrip('\n')))
        return set(sorted(words))

    def get_prefixes(self):
        """Get possible prefixes from words"""
        prefixes = []
        for word in self._words:
            for idx in range(2, len(word) + 1):
                prefixes.append(word[:idx])
        return set(sorted(prefixes))

    def adjacencies(self, coord):
        """Finds adjacent letters in word list"""
        x, y = coord
        for adj_x in range(max(0, x - 1), min(x + 2, self._cols)):
            for adj_y in range(max(0, y - 1), min(y + 2, self._rows)):
                yield (adj_x, adj_y)

    def find_word(self, pfix, adj_path):
        """Uses the prefix to find potential words"""
        # If prefix found use the prefix
        if pfix in self._words:
            yield(pfix, adj_path)
        for (adj_x, adj_y) in self.adjacencies(adj_path[-1]):
            if (adj_x, adj_y) not in adj_path:
                cur_pfix = pfix + self._game_grid[adj_y][adj_x]
                if cur_pfix in self._prefixes:
                    for result in self.find_word(
                            cur_pfix,
                            adj_path + ((adj_x, adj_y),)
                            ):
                        yield result

    def find_solutions(self):
        """Narrows the dictionary word list to words that can only exist
        adjacently on the game board"""
        for y, row in enumerate(self._game_grid):
            for x, letter in enumerate(row):
                for result in self.find_word(letter, ((x, y),)):
                    yield result

    def get_answers(self):
        """Creates and returns a list of possible board answers"""
        ans = []
        for (word, path) in self.find_solutions():
            ans.append(word)
        ans = list(set(sorted(ans)))
        return ans

    def __str__(self):
        output = ""
        for row in self._game_grid:
            for idx in range(0, len(row)):
                output += "[{0}]".format(row[idx])
            output += "\n"
        return output
